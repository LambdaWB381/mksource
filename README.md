# MkSource
***  
## `Introduction`
This program generate *C* `head file` and `source file` by name.  
You can copy this program to your `$PATH` ou your *OS*.  

## `Example Use:`
`#> mkdource -Hn -Sn easyuse`  

## `Program Output`  
### *`filename.h`*  
*`with -Hn`*  
```
#ifndef FILENAME_H
#define FILENAME_H



#endif
```    

*`with -Hcpp`*  
```
#ifndef FILENAME_H
#define FILENAME_H



#ifdef __cplusplus
extern "C" {
#endif



#ifdef __cplusplus
}
#endif
#endif
```  
***  
*`filename.c`*  
*`with -SN`*  
```
#include "filename.h"
  
```  

*`with -Si`*    
```
#include <stdio.h>
#include "filename.h"
  
```  

*`with -Sl`*  
```
#include <stdlib.h>
#include "filename.h"
  
```  

*`with -Sb`*  
```
#include <stdio.h>
#include <stdlib.h>
#include "filename.h"
  
```  
***
